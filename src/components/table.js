import React from "react";
import { Icon, Pagination, Menu, Table, Loader } from "semantic-ui-react";

class DataTable extends React.Component {
  state = {
    ready: false
  };

  componentDidMount() {
    this.props.getData().then(res => {
      this.setState({ ...res.data, ready: true });
    });
  }

  handlePaginationChange = (e, { activePage }) => {
    this.setState({ page: activePage - 1, ready: false });
    this.props.getData({ page: activePage - 1 }).then(res => {
      this.setState({ ...res.data, ready: true });
    });
  };

  render() {
    if (!this.state.ready && this.state.page === undefined) {
      return <Loader active inline="centered" />;
    }
    return (
      <Table celled>
        {this.state.ready ? (
          <React.Fragment>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Index</Table.HeaderCell>

                {this.state.data.columns.map(col => (
                  <Table.HeaderCell key={col}>{col}</Table.HeaderCell>
                ))}
              </Table.Row>
            </Table.Header>

            <Table.Body>
              {this.state.data.data.map((row, i) => (
                <Table.Row key={i}>
                  <Table.Cell>{this.state.data.index[i]}</Table.Cell>

                  {row.map((value, i) => (
                    <Table.Cell key={i}>{value}</Table.Cell>
                  ))}
                </Table.Row>
              ))}
            </Table.Body>
          </React.Fragment>
        ) : (
          <Table.Body>
            <Table.Row>
              <Table.Cell>
                <Loader active inline="centered" />
              </Table.Cell>
            </Table.Row>
          </Table.Body>
        )}

        {this.state.page !== undefined ? (
          <Table.Footer>
            <Table.Row>
              <Table.HeaderCell colSpan={this.state.data.index.length}>
                <div className="pagination">
                  <Pagination
                    activePage={this.state.page + 1}
                    onPageChange={this.handlePaginationChange}
                    ellipsisItem={{
                      content: <Icon name="ellipsis horizontal" />,
                      icon: true
                    }}
                    firstItem={{
                      content: <Icon name="angle double left" />,
                      icon: true
                    }}
                    lastItem={{
                      content: <Icon name="angle double right" />,
                      icon: true
                    }}
                    prevItem={{
                      content: <Icon name="angle left" />,
                      icon: true
                    }}
                    nextItem={{
                      content: <Icon name="angle right" />,
                      icon: true
                    }}
                    totalPages={this.state.pages}
                  />
                </div>
              </Table.HeaderCell>
            </Table.Row>
          </Table.Footer>
        ) : (
          false
        )}
      </Table>
    );
  }
}

export default DataTable;
