import { observable, action, decorate } from "mobx";
import { persist, create } from "mobx-persist";
import axios from "axios";

class Auth {
  secret = null;
  key = null;

  user = null;

  setAuth = ({ key, secret }) => {
    this.key = key;
    this.secret = secret;

    axios.defaults.headers.common["api-key"] = key;
    axios.defaults.headers.common["api-secret"] = secret;

    axios.get("/user").then(res => {
      this.setUser(res.data);
      console.log(res);
    });
  };

  setUser = user => {
    this.user = user;
  };

  logout = () => {
    this.user = null;
    this.secret = null;
    this.key = null;

    delete axios.defaults.headers.common["api-key"];
    delete axios.defaults.headers.common["api-secret"];
  };
}

decorate(Auth, {
  secret: [persist, observable],
  key: [persist, observable],
  user: [persist("object"), observable],

  setAuth: action,
  setUser: action,
  logout: action
});

const hydrate = create({
  storage: localStorage
});

const store = new Auth();
hydrate("auth", store)
  
// This is dull, but I don't have time
const persisted = JSON.parse(localStorage.getItem('auth'))
if (persisted && persisted.secret) {
  axios.defaults.headers.common["api-key"] = persisted.key;
  axios.defaults.headers.common["api-secret"] = persisted.secret;  
}

export default store;
