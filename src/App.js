import React, { Component } from "react";
import { Auth } from "./containers";
import { Container, Header, Menu, Segment, Tab } from "semantic-ui-react";
import "./App.scss";
import stores from "./stores";
import { Provider } from "mobx-react";

class App extends Component {
  render() {
    return (
      <Provider {...stores}>
        <Auth auth={stores.auth} />
      </Provider>
    );
  }
}

export default App;
