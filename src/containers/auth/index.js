import React from "react";
import { inject, observer } from "mobx-react";
import { Home, SignIn } from "../";

class Auth extends React.Component {
  render() {
    return !!this.props.auth.secret ? <Home /> : <SignIn />;
  }
}

export default observer(Auth);
