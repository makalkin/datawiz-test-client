import React from "react";
import { inject, observer } from "mobx-react";
import {
  Container,
  Header,
  Menu,
  Segment,
  Tab,
  Button,
  Form as SemanticForm
} from "semantic-ui-react";
import { Form, Field } from "simple-react-form";
import axios from "axios";

import "./styles.scss";

const Text = ({ value, onChange, label }) => (
  <SemanticForm.Input
    label={label}
    type="email"
    value={value}
    onChange={e => onChange(e.target.value)}
  />
);

const Password = ({ value, onChange, label }) => (
  <SemanticForm.Input
    label={label}
    type="password"
    value={value}
    onChange={e => onChange(e.target.value)}
  />
);

class SignIn extends React.Component {
  state = {
    email: "",
    password: ""
  };

  onSubmit = data => {
    axios
      .post("/signin", data)
      .then(res => {
        this.props.auth.setAuth({
          key: res.data.API_KEY,
          secret: res.data.API_SECRET
        });
        return res.data;
      })
      .catch(err => this.showError(err));
  };

  showError = err => {
    console.log(err);
    // TODO: show
  };

  render() {
    return (
      <div className="signin-container">
        <Form
          ref={ref => (this.form = ref)}
          state={this.state}
          onChange={state => this.setState(state)}
          onSubmit={this.onSubmit}
          className="ui form"
        >
          <Field fieldName="email" label="Email" type={Text} />
          <Field fieldName="password" label="Password" type={Password} />

          <Button type="submit">Submit</Button>
        </Form>
      </div>
    );
  }
}

export default inject("auth")(observer(SignIn));
