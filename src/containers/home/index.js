import React, { Component } from "react";
import { DataTable } from "../../components";
import {
  Container,
  Header,
  Menu,
  Segment,
  Tab,
  Image,
  Button,
  Loader
} from "semantic-ui-react";
import { observer, inject } from "mobx-react";

import "./styles.scss";

import axios from "axios";

class SalesService {
  getSales = () => {
    return axios.get("/sales");
  };

  getGoods = params => {
    console.log(params)
    return axios.get("/goods", { params });
  };
}

class Home extends Component {
  constructor(props) {
    super(props);
    this.salesService = new SalesService();
  }

  render() {
    return (
      <Container className="home-container">
        <Menu fixed="top" inverted>
          <Container>
            <Menu.Item as="a" header>
              <Image
                size="mini"
                circular
                src="https://react.semantic-ui.com/images/avatar/large/patrick.png"
                style={{ marginRight: "1.5em" }}
              />
              {this.props.auth.user ? (
                this.props.auth.user.name
              ) : (
                <Loader active inline="centered" />
              )}
            </Menu.Item>
            <Menu.Item
              position="right"
              as="a"
              onClick={() => {
                this.props.auth.logout();
              }}
            >
              Logout
            </Menu.Item>
          </Container>
        </Menu>
        <Header as="h1"> Your dashboard, kinda </Header>
        <DataTable getData={this.salesService.getSales} />
        <Header as="h1"> Goods and stats </Header>
        <DataTable getData={this.salesService.getGoods} />
      </Container>
    );
  }
}

export default inject(stores => ({ auth: stores.auth }))(observer(Home));
